/* 
Terraform Code to Build out Custom VPC and EC2 Instances.
Contains AWS Account Info
awsconfig.tf

Johnga 10/21/2018
*/

# Define AWS as our provider
provider "aws" {
  region = "${var.aws_region}"
  shared_credentials_file = "${var.shared_credentials_file}"
  profile                 = "${var.profile}"
}
